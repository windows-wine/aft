![](ss_aft0003.png)

AFT (Autorun Fixer and Tweaker) was designed for old Windows® versions like 95/98/ME/2000/XP.       
**It is not recommended to apply any of its tweaks in versions of Windows newer than XP.**

Its initial function was to allow disabling of the autorun feature which at some point      
had gotten abused of to install malware when removable media (optical disc, USB storage etc)        
was inserted. A few more useful options have been added along the way.

This is now a legacy tool for retro hobbyists. Please treat it as such.

#### Brief explanation of the functionality
The checkboxes appearing in an undetermined state - neither checked nor cleared - mean      
their respective registry keys have not yet been created. Once a checkbox has been toggled      
and the changes have been applied there is no way to revert to an undetermined state other      
than manually editing the registry and deleting the respective key.

The application automatically saves the current state of the registry keys before modifying them.       
However, due to logical failure the backup file will be overwritten on subsequent changes.      
Therefore it is recommended that the backup file called **values.txt** present in current folder        
be renamed or moved to a safe location, should one revert the changes at a later time.      
This bug may be corrected in a future version, should there be one.

The code has been written for **AutoHotkey** Basic (v**1.0**).

AFT is released as freeware open-source under the GNU Public License v2 or later.       
The icon has been taken from some package on the web long ago, forgot the source.       
It should retain its creator's license.

**© Drugwash, 2010-2023**
